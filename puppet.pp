# puppet/manifests/init.pp

package { 'nginx':
  ensure => 'installed',
}

service { 'nginx':
  ensure  => 'running',
  enable  => true,
  require => Package['nginx'],
}

# Add other Puppet configurations as needed
