provider "aws" {
region = "us-east-1"
}
//vpc syntax//
resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "my_vpc"
  }
}
//subnet//
resource "aws_subnet" "my_subnet" {
  vpc_id     = aws_vpc.my_vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name = "my_subnet"
  }
}
#define security group
resource "aws_security_group" "my_security_group" {
  vpc_id = aws_vpc.my_vpc.id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }
    tags = {
    Name = "my_security_group"
  }
}


#define EC2 instance

resource "aws_instance" "my_instance" {
  ami   = "ami-0440d3b780d96b29d"
  instance_type = "t2.micro"
  subnet_id =aws_subnet.my_subnet.id
  key_name = "myec2key"

  tags={
    Name = "my_instance"
  }
}