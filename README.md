1. Clone the Repository

git clone https://gitlab.com/Apsar2629/puppetterraformintegrations.git
cd PuppetTerraformIntegration


2. Terraform Configuration

cd terraform
terraform init
terraform validate
terraform plan
terraform apply

3. Puppet Configuration

puppet apply puppet.pp

#Configuration Details
#Terraform Configuration
VPC and Subnet
The Terraform code defines a VPC with the CIDR block "10.0.0.0/16" and a subnet within that VPC with the CIDR block "10.0.1.0/24" in the "us-east-1a" availability zone.
Security Group
The security group allows all outbound traffic and can be customized as needed.
EC2 Instance
An EC2 instance is provisioned with the specified AMI and instance type, associated with the created subnet, and uses the specified key pair.



#Puppet Configuration
Nginx Installation
Puppet manifests in puppet.pp ensure that Nginx is installed and its service is running.
